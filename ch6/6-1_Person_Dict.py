#6-1. Person: 
'''Use a dictionary to store information about a person you know. 
Store their first name, last name, age, and the city in which they live. 
You should have keys such as first_name, last_name, age, and city. 
Print each piece of information stored in your dictionary.'''
###
dic={"fname":"rich", "lname":"glazier", "age":50, "city":"denver"}
print(dic)
#########################
## EXTRA CREDIT
#########################
## Print entire dictionary in an fstring with newline
print('\n\tEXTRA CREDIT\n\t*Print entire dictionary as-is within an fstring. Include new line.')
print(f"{dic}\n")
## Print dictionary with get method and it's argument for key to return
print("\t-Print dictionary with 'get' method and it's argument for key to return.")
print(dic.get("fname"))
## Print dictionary with squre bracket index location for key to return
print("\t-Print dictionary with squre bracket index location for key to return.")
print(dic["fname"])
## Print dictionary with get method and it's argument for key to return, and exection handle for no key
print("\t-Print dictionary with 'get' method and it's argument for key to return,\n\tand include \
exeption handle for no key. This example has a vaid key.")
print(dic.get("fname", "He's not in here"))
## Print dictionary with get method and it's argument for key to return, and exection handle for no key
print("\t-Print dictionary with 'get' method and it's argument for key to return,\n\tand include \
exeption handle for no key. This example has an invaid key.")
print(dic.get("firstname", "He's not in here"))
