import json
#6-2. Favorite Numbers: 
'''Use a dictionary to store people’s favorite numbers. 
Think of five names, and use them as keys in your dictionary. 
Think of a favorite number for each person, and store each as a value in your dictionary. 
Print each person’s name and their favorite number. 
For even more fun, poll a few friends and get some actual data for your program.'''
#########
d={"dave":42, "rj":42, "corey":1, "rich":16, "brad":25}
#########
print(json.dumps(d,seperators=("; ","=")),"\n")
print(d, "\n")
print(json.dumps(d,separators=(" ","\t"), indent=0),"\n")

########################################
## CONVERT TO STRING, SO I CAN REMOVE {}
########################################
nd=(json.dumps(d,separators=(" ","\t"), indent=0))
stnd=str(nd)
stnd.replace("{", "")
stnd.replace("}", "")
print(stnd.replace("{", "").replace("}", ""))
###################
#### OUTPUT EXAMPLE
###################
#{"dave"=42; "rj"=42; "corey"=1; "rich"=16; "brad"=25} 
#
#{'dave': 42, 'rj': 42, 'corey': 1, 'rich': 16, 'brad': 25} 
#
#{
#"dave"	42 
#"rj"	42 
#"corey"	1 
#"rich"	16 
#"brad"	25
#} 
#
#
#"dave"	42 
#"rj"	42 
#"corey"	1 
#"rich"	16 
#"brad"	25
#
