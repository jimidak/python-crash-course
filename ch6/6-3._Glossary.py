#6-3. Glossary: 
'''A Python dictionary can be used to model an actual dictionary. 
However, to avoid confusion, let’s call it a glossary.'''
# - Think of five programming words you’ve learned about in the previous chapters. 
# Use these words as the keys in your glossary, and store their meanings as values.
#
# - Print each word and its meaning as neatly formatted output. 
# You might print the word followed by a colon and then its meaning, or print the word on one line 
# and then print its meaning indented on a second line. 
# Use the newline character (\n) to insert a blank line between each word-meaning pair in your output.
#####
## Brainstorming terms I've learned
## method, lstrip, append, pop, range, slice
#####
## "method" : "a function that is associated with a class. I think of a method as something that manipulates"
## "lstrip" : "a method for manipulating a string. It removes leading white space"
## "append" : "a method for manipulating a list. It adds a new index to the end of the list"
## "pop" : "a method for removing an item from a list. It both removes it, and returns it's output, unlike the remove method"
## "range" : "a function to generate a sequence of numbers, that is an iterable data type called 'range object'""
terms = {
"method" : "a function that is associated with a class. I think of a method as something that manipulates.",
"lstrip" : "a method for manipulating a string. It removes leading white space.",
"append" : "a method for manipulating a list. It adds a new index to the end of the list.",
"pop" : "a method for removing an item from a list. It both removes it, and returns it's output, unlike the remove method.",
"range" : "a function to generate a sequence of numbers, that is an iterable data type called 'range object'."
}
for k in terms:
	print(k, f"\t:", terms[k])