#6-4. Glossary 2: 
# Now that you know how to loop through a dictionary, 
# clean up the code from Exercise 6-3 (page 99) 
# by replacing your series of print() calls with a loop that runs through the dictionary’s keys and values. 
# When you’re sure that your loop works, 
# add five more Python terms to your glossary. 
# When you run your program again, these new words and meanings should automatically be included in the output.
##############################
# Old 6-3 Glossary code
##############################
#terms = {
#"method" : "a function that is associated with a class. I think of a method as something that manipulates.",
#"lstrip" : "a method for manipulating a string. It removes leading white space.",
#"append" : "a method for manipulating a list. It adds a new index to the end of the list.",
#"pop" : "a method for removing an item from a list. It both removes it, and returns it's output, unlike the remove method.",
#"range" : "a function to generate a sequence of numbers, that is an iterable data type called 'range object'."
#}
#for k in terms:
#	print(k, f"\t:", terms[k])
##############################
terms = {
	"method" : "a function that is associated with a class. I think of a method as something that manipulates.",
	"lstrip" : "a method for manipulating a string. It removes leading white space.",
	"append" : "a method for manipulating a list. It adds a new index to the end of the list.",
	"pop" : "a method for removing an item from a list. It both removes it, and returns it's output, unlike the remove method.",
	"range" : "a function to generate a sequence of numbers, that is an iterable data type called 'range object'."
}
def dicfunc():
    for k, v in terms.items():
        print(f"{k} \t: {v}")
#
terms.update({"PEP8":"programming best practices for Python", "update":"method to add key:value pair(s)", "None":"similar ton null. A placeholder", "set":"an unordered list", "sep":"argument or parameter to the print() function, whereby you can change the seperator from 1space, when multiple items are in a print statement"})
dicfunc()
##############################
### OUTPUT EXAMPLE
##############################
'''
method 	: a function that is associated with a class. I think of a method as something that manipulates.
lstrip 	: a method for manipulating a string. It removes leading white space.
append 	: a method for manipulating a list. It adds a new index to the end of the list.
pop 	: a method for removing an item from a list. It both removes it, and returns it's output, unlike the remove method.
range 	: a function to generate a sequence of numbers, that is an iterable data type called 'range object'.
PEP8 	: programming best practices for Python
update 	: method to add key:value pair(s)
None 	: similar ton null. A placeholder
set 	: an unordered list
sep 	: argument or parameter to the print() function, whereby you can change the seperator from 1space, when multiple items are in a print statement
'''