#6-5. Rivers: 
#Make a dictionary containing three major rivers and the country each river runs through. 
#One key-value pair might be 'nile': 'egypt'.
'''
• Use a loop to print a sentence about each river, such as The Nile runs through Egypt.
• Use a loop to print the name of each river included in the dictionary.
• Use a loop to print the name of each country included in the dictionary.
'''
rivers={"Mississippi":"USA", "Yello":"China", "Thames":"England"}
for i, j in rivers.items():
	if i == "Mississippi":
		print(f"The {i} is a river in {j} that was central to Mark Twain novels.")
	if i == "Yello":
		print(f"The {i} is the largest river in Asia, and is located in {j}.")
	if i == "Thames":
		print(f"The {i} winds through London, {j}.")
#############
## OUTPUT EX.
#############
#The Mississippi is a river in USA that was central to Mark Twain novels.
#The Yello is the largest river in Asia, and is located in China.
#The Thames winds through London, England.
#############
## ALTERNATE
#############
#
rivers={"Mississippi":"USA", "Yello":"China", "Thames":"England"}
for i, j in rivers.items():
		print(i)
		print(f"\t{j}")
		print()
#############
## OUTPUT EX.
#############
#Mississippi
#	USA
#
#Yello
#	China
#
#Thames
#	England
#