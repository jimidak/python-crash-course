#6-6. Polling: 
#Use the code in favorite_languages.py (page 97).
'''
• Make a list of people who should take the favorite languages poll. Include
some names that are already in the dictionary and some that are not.

• Loop through the list of people who should take the poll. If they have already 
taken the poll, print a message thanking them for responding. 
If they have not yet taken the poll, print a message inviting them to take the poll.
'''
people=["david", "rj", "brad", "sarah", "brian", "silvia", "jen", "corey"]

favorite_editor = {
       'jen': 'word',
       'sarah': 'notepad++',
       'david': 'atom',
       'silvia': 'vs_code',
       'rich': 'sublime'
       }
editor = favorite_editor['sarah'].title() 
print(f"Sarah's favorite language is {editor}.")